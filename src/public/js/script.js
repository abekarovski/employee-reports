$(function () {
    $("body").on('click', '.delete-confirmation', function (e) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $(this).closest('form').submit();

            }
        })
    });

    $("body").on('change', '#question_type', function (e) {
        if ($(this).val() == 'select') {
            $('#question_select_group_div').prop('hidden', false);
            $('#question_select_group').prop('disabled', false);
        } else {
            $('#question_select_group_div').prop('hidden', true);
            $('#question_select_group').prop('disabled', true);
        }
    });

    $("body").on('click', '.delete-grandparent', function (e) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $(this).parent().parent().remove();
            }
        })
    });

    $("body").on('click', '#create_report', function () {
        $('#report_submitted').val(0);
        $('#create_report_form').submit();
    });

    $("body").on('click', '#submit_report', function () {
        $('#report_submitted').val(1);
        $('#create_report_form').submit();
    });

    $("body").on('click', '#add_question_select', function (e) {
        var label = $('#question_select_name').val();
        var value = $('#question_select_value').val();

        if (label == '') {
            Swal.fire({
                title: 'Error!',
                text: 'Please enter the label!',
                icon: 'error',
            });
            return false;
        }
        if (value == '' || !jQuery.isNumeric(value)) {
            Swal.fire({
                title: 'Error!',
                text: 'Please enter the correct value(numbers only)!',
                icon: 'error',
            });
            return false;
        }

        var fail = 0;
        $('.question-select-value').each(function () {
            if ($(this).val() == $('#question_select_value').val()) {
                fail = 1;

                return false;
            }
        });

        if (fail == 1) {
            Swal.fire({
                title: 'Error!',
                text: 'Please enter different values for the select!',
                icon: 'error',
            });

            return false;
        }


        var html =
            '        <div class="form-row">\n' +
            '            <div class="form-group col-md-5">\n' +
            '                <input type="text" class="form-control" value="' + label + '" name="question_select_label[]" readonly>\n' +
            '            </div>\n' +
            '            <div class="form-group col-md-5">\n' +
            '                <input type="text" class="form-control question-select-value" value="' + value + '" name="question_select_value[]" readonly>\n' +
            '            </div>\n            ' +
            '           <div class="form-group col-md-2">\n' +
            '                <button class="btn btn-outline-danger delete-grandparent" type="button">Delete</button>\n' +
            '            </div>' +

            '        </div>';

        $('#add_question_selects_div').after(html);

    });

    $('body').on('change', '#copy_from_week_admin_reports', function () {
        $.ajax({
            type: 'POST',
            url: $(this).data('url'),
            data: {
                id: $(this).val(),
                _token: token
            },
            dataType: 'JSON'
        }).done(function (questions) {
            $('.questions-checkboxes').prop('checked', false);
            $.each(questions, function (idx, value) {
                $('#question_checkbox_' + value.id).prop('checked', true);
            });
        });
    });

    $('body').on('change', '#copy_from_week_employee_reports', function () {
        var date = $(this).val();
        $.ajax({
            type: 'POST',
            url: $(this).data('url'),
            data: {
                date_week: date,
                _token: token
            }
        }).done(function (view) {
            if (view == 'submitted') {
                Swal.fire({
                    title: 'Error!',
                    text: 'You have already submitted a report for that week!',
                    icon: 'error',
                    confirmButtonText: 'OK'
                })
            } else {
                $('#report_date').val(date)
                $('#report_answers_div').html(view);
            }
        });
    });

    $("body").on("click", ".role-permissions-button", function (e) {
        $('#active_role_id').val($(this).data('role_id'));
        $('.active-row').removeClass('bg-info');
        $(this).parent().parent().parent().addClass('bg-info');
        $.ajax({
            type: 'POST',
            url: $(this).closest('form').attr('action'),
            data: $(this).closest('form').serialize(),
            dataType: 'JSON'
        }).done(function (permissions) {
            $('.role-permissions').prop('checked',false);
            $.each(permissions, function (index, value) {
                var permission_id = value.id;
                $('.role-permissions').each(function () {
                    if ($(this).val() == permission_id){
                        $(this).prop('checked', true);
                    }
                });
            });
        });
    });

    $("body").on("click",".user-role-permission", function () {
        $.ajax({
            type: 'POST',
            url: $(this).data('url'),
            data: {
                'id' : $(this).val(),
                'user_id' : $(this).data('user_id'),
                '_token' : token,
                'save' : $(this).prop('checked'),
            }
        }).done(function () {

        });
    });

    $("body").on("click",".role-permissions", function () {
        $.ajax({
            type: 'POST',
            url: $(this).data('url'),
            data: {
                'permission_id' : $(this).val(),
                'role_id' : $('#active_role_id').val(),
                '_token' : token,
                'save' : $(this).prop('checked'),
            }
        }).done(function () {

        });
    });

    $("body").on('click', '#add_holidays', function (e) {
        var date = $('#holiday_date').val();

        if (date == '') {
            Swal.fire({
                title: 'Error!',
                text: 'Please enter the date!',
                icon: 'error',
            });
            return false;
        }

        var html = '            <div class="form-row"><div class="form-group col-md-5">\n' +
            '                <input type="text" class="form-control" name="name[]" autocomplete="off" value="' + $('#holiday_name').val() + '" readonly>\n' +
            '            </div>\n' +
            '            <div class="form-group col-md-4">\n' +
            '                <input type="text" class="form-control date-year-month-day" name="date[]" required autocomplete="off" value="' + date + '" readonly/>\n' +
            '            </div>\n' +
            '            <div class="form-group col-md-3">\n' +
            '                <input type="text" class="form-control" name="type[]" required autocomplete="off" value="' + $('#holiday_type').val() + '" readonly/>\n' +
            '            </div></div>';

        $('#add_holidays_div').append(html);

    });

    $("body").on('click', '#add_remainder_time', function (e) {
        console.log('ye')
        var remainder_oprator = $('#remainder_oprator').val();
        var remainder_hours = $('#remainder_hours').val();

        if (!jQuery.isNumeric(remainder_hours)) {
            Swal.fire({
                title: 'Error!',
                text: 'Please enter the number of hours!',
                icon: 'error',
            });
            return false;
        }

        var html = '            <div class="form-row"><div class="form-group col-md-5">\n' +
            '                <input type="text" class="form-control" autocomplete="off" value="' + remainder_oprator + '" name="operator[]" readonly/>\n' +
            '            </div>\n' +
            '            <div class="form-group col-md-5">\n' +
            '                <input type="text" class="form-control" autocomplete="off" value="' + remainder_hours + '" name="hours[]" readonly/>\n' +
            '            </div>' +
            '           <div class="form-group col-md-2">\n' +
            '                        <button class="btn btn-danger delete-grandparent" type="button">Delete</button>\n' +
            '                    </div>' +
            '</div>';

        $('#add_remainder_time_div').append(html);

    });

    $('body').on('submit', '.submit-modal', function (e) {
        e.preventDefault();

        var url = $(this).closest('form').attr('action');
        var message = $(this).data('message');

        var data = $(this).closest('form').serialize()+ '&_token=' + token;
        $.ajax({
            type: 'POST',
            url : url,
            data: data,
            statusCode: {
                422: function(message) {
                    Swal.fire({
                        title: 'Error!',
                        text: message.responseJSON.errors.name,
                        icon: 'error',
                    });
                }
            }
        }).done(function () {
            Swal.fire({
                title: 'Success!',
                text: message,
                icon: 'success',
            });

            location.reload(true);
        });
    });

    $('.date-year-week').datepicker({
        onSelect: function (dateText, inst) {
            $(this).val("");
            var week = $.datepicker.iso8601Week(new Date(dateText));
            var year = new Date(dateText).getFullYear();
            $(this).val(year + '-' + week);
        }
    });

    $('.date-year-month-day').datepicker({
        autoOpen: false,
        changeMonth: true,
        firstDay: 1,
        changeYear: true,
        dateFormat: 'yy-mm-dd'
    });
});

var GlobalClass = {};

GlobalClass.ajaxDataseCall = function (url, data) {
    $.ajax({
        type: 'POST',
        data: data,
        url: url
    }).done(function () {
        $("#list_questions").bind('click', function () {
            window.location.href = $(this).attr('href');
        });

        $('#list_questions').click();
    });
}

GlobalClass.openModal = function(url, data, max_width = '') { // function that opens a bootstrap modal
    $('#loading').css({'display' : 'block'});
    if (data == ''){
        var data = {};
        data = {
            _token: token
        }
    }else{
        data._token = token;
    }

    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        dataType: 'json'
    }).done(function (html) {
        $('.display-modal').empty();
        $('.display-modal').html(html);
        if (max_width != ''){
            $('.modal-dialog').css({'max-width':max_width});
        }else{
            $('.modal-dialog').css({'max-width':'500px'});
        }
        $('#myModal').modal('show');
        $('#loading').css({'display' : 'none'});
    });
};

