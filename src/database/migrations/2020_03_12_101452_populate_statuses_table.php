<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PopulateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $status = new \App\Status;

        $status->name = 'In Progress';
        $status->code = 'in_progress';

        $status->save();
        $status = new \App\Status;

        $status->name = 'Submitted';
        $status->code = 'submitted';

        $status->save();
        $status = new \App\Status;

        $status->name = 'Remainder sent';
        $status->code = 'remainder_sent';

        $status->save();

        $status = new \App\Status;

        $status->name = 'Approved';
        $status->code = 'approved';

        $status->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Status::truncate();
    }
}
