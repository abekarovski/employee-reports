<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $employee = \Spatie\Permission\Models\Role::create(['name' => 'Employee']);//Only employee permissions
        \Spatie\Permission\Models\Role::create(['name' => 'Super Admin']);//Has all permissions
        $permissionEmployee = [];

        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'users.send_application_invite']);
        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'users.resource_routes']);
        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'users.tab']);
        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'questions.tab']);
        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'questions.resource_routes']);
        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'question_selects.resource_routes']);
        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'holidays.tab']);
        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'holidays.resource_routes']);
        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'reports.tab']);

        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'reports.list_own_reports']);
        array_push($permissionEmployee, $permission);

        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'reports.submit_report']);
        array_push($permissionEmployee, $permission);

        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'reports.create_employee_report']);
        array_push($permissionEmployee, $permission);

        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'reports.copy_employee_report_questions']);
        array_push($permissionEmployee, $permission);

        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'reports.copy_admin_report_questions']);
        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'reports.change_report_submission_time']);
        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'reports.approve_report']);
        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'reports.resource_routes']);
        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'admin.tab']);
        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'admin.application_access_control']);
        $permission = \Spatie\Permission\Models\Permission::create(['name' => 'admin.users_access_control']);

        $employee->givePermissionTo($permissionEmployee);

        $user = new \App\User;

        $user->name = 'Super Admin';
        $user->email = 'admin@admin.com';
        $user->password = '$2y$10$ISvQxvQRULhtAcrbsj.qJeZb0QQVLxNrn30INrOVsex.0AFD8BuaO';

        $user->save();

        $user->assignRole('Super Admin');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
