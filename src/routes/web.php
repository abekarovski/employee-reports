<?php

use App\QuestionSelectGroup;
use App\Report;
use Illuminate\Support\Facades\Mail;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades;
use App\Mail\ReportMail;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users/create_invitation', 'UsersController@create_invitation')->name('users.create_invitation')->middleware('permission:users.send_application_invite');
Route::post('/users/send_invitation', 'UsersController@send_invitation')->name('users.send_invitation')->middleware('permission:users.send_application_invite');

Route::resource('users', 'UsersController')->middleware('permission:users.resource_routes');

Route::post('/questions/activate_question', 'QuestionsController@activate_question')->name('questions.activate_question')->middleware('permission:questions.resource_routes');

Route::get('/questions/create_select_group', 'QuestionsController@create_select_group')->name('questions.create_select_group')->middleware('permission:question_selects.resource_routes');
Route::get('/questions/{question_select_group}/edit_select_group', 'QuestionsController@edit_select_group')->name('questions.edit_select_group')->middleware('permission:question_selects.resource_routes');
Route::post('/questions/store_question_select_group', 'QuestionsController@store_question_select_group')->name('questions.store_question_select_group')->middleware('permission:question_selects.resource_routes');
Route::put('/questions/{question_select_group}', 'QuestionsController@update_question_select_group')->name('questions.update_question_select_group')->middleware('permission:question_selects.resource_routes');

Route::resource('questions', 'QuestionsController')->middleware('permission:questions.resource_routes');

Route::get('/reports/index_own_reports', 'ReportsController@index_own_reports')->name('reports.index_own_reports')->middleware('permission:reports.list_own_reports');
Route::get('/reports/{report}/submit_report', 'ReportsController@submit_report')->name('reports.submit_report')->middleware('permission:reports.submit_report');
Route::get('/reports/create_employee_report', 'ReportsController@create_employee_report')->name('reports.create_employee_report')->middleware('permission:reports.create_employee_report');
Route::post('/reports/get_report_questions', 'ReportsController@get_report_questions')->name('reports.get_report_questions')->middleware('permission:reports.copy_admin_report_questions');
Route::post('/reports/get_report_employee_questions', 'ReportsController@get_report_employee_questions')->name('reports.get_report_employee_questions')->middleware('permission:reports.copy_employee_report_questions');
Route::get('/reports/change_submission_time', 'ReportsController@change_submission_time')->name('reports.change_submission_time')->middleware('permission:reports.change_report_submission_time');
Route::post('/reports/store_submission_time', 'ReportsController@store_submission_time')->name('reports.store_submission_time')->middleware('permission:reports.change_report_submission_time');
Route::post('/reports/approve_report', 'ReportsController@approve_report')->name('reports.approve_report')->middleware('permission:reports.approve_report');

Route::resource('reports', 'ReportsController')->middleware('permission:reports.resource_routes');

Route::resource('holidays', 'HolidaysController')->only([
    'index', 'create', 'store', 'destroy'
])->middleware('permission:holidays.resource_routes');

Route::get('/access_control/index', 'AccessControlController@index')->name('access_control.index')->middleware('permission:admin.application_access_control');
Route::post('/access_control/role_permissions', 'AccessControlController@role_permissions')->name('access_control.role_permissions')->middleware('permission:admin.application_access_control');
Route::post('/access_control/save_role_permission', 'AccessControlController@save_role_permission')->name('access_control.save_role_permission')->middleware('permission:admin.application_access_control');
Route::post('/access_control/create_role', 'AccessControlController@create_role')->name('access_control.create_role')->middleware('permission:admin.application_access_control');
Route::post('/access_control/store_role', 'AccessControlController@store_role')->name('access_control.store_role')->middleware('permission:admin.application_access_control');
Route::delete('/access_control/destroy_role/{role_id}', 'AccessControlController@destroy_role')->name('access_control.destroy_role')->middleware('permission:admin.application_access_control');
Route::post('/access_control/create_permission', 'AccessControlController@create_permission')->name('access_control.create_permission')->middleware('permission:admin.application_access_control');
Route::post('/access_control/store_permission', 'AccessControlController@store_permission')->name('access_control.store_permission')->middleware('permission:admin.application_access_control');
Route::delete('/access_control/destroy_permission/{permission_id}', 'AccessControlController@destroy_permission')->name('access_control.destroy_permission')->middleware('permission:admin.application_access_control');
Route::get('/access_control/{user}/edit_users_access_control', 'AccessControlController@edit_users_access_control')->name('access_control.edit_users_access_control')->middleware('permission:admin.users_access_control');
Route::post('/access_control/save_user_role', 'AccessControlController@save_user_role')->name('access_control.save_user_role')->middleware('permission:admin.users_access_control');
Route::post('/access_control/save_user_permission', 'AccessControlController@save_user_permission')->name('access_control.save_user_permission')->middleware('permission:admin.users_access_control');




Route::get('/roles', function (){
    \App\User::find(1)->assignRole('Super Admin');
});




