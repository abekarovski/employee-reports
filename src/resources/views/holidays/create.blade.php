@extends('layouts.master')

@section('content')
    <div class="row p-lg-2 pt-5">
        <div class="col-sm-10 offset-1">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Add holiday</h3>
                </div>
                <form method="post" action="{{route('holidays.store')  }}">
                    @csrf
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <label for="holiday_name">Holiday name:</label>
                                <input type="text" class="form-control" id="holiday_name" autocomplete="off">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="holiday_date">Date:</label>
                                <input type="text" class="form-control date-year-month-day" id="holiday_date" required autocomplete="off"/>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="holiday_type">Type:</label>
                                <select class="form-control" id="holiday_type">
                                    <option value="global">Global</option>
                                    <option value="yearly">Yearly</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div id="add_holidays_div">

                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary float-right m-1">Save</button>
                            <button type="button" class="btn btn-primary float-right m-1" id="add_holidays">Add</button>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </form>
            </div>
        </div>
    </div>
@endsection
