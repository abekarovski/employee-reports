@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-sm-8 offset-2">
            <h2 class="pt-5 pb-2">Holidays({{ date('Y') }})</h2>
            <table class="table jquery-datatable">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Date</th>
                    <th scope="col">Type</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;  foreach($holidays as $key => $value){ ?>
                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $value->name }}</td>
                    <td>{{ date('Y').'-'.$value->month_day }}</td>
                    <td>{{ ucfirst($value->type) }}</td>
                    <td>
                        <form method="POST" action="{{ route('holidays.destroy', ['holiday' => $value->id]) }}">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger delete-confirmation" type="button">Delete</button>
                        </form>
                    </td>
                </tr>
                <?php $i++; } ?>
                </tbody>
            </table>
        </div>
    </div>
@endsection
