@extends('layouts.master')

@section('content')
    <input hidden id="active_role_id" value="">
    <div class="row">
        <div class="col-sm-6">
            <h2 class="align-items-center">Roles<button class="btn btn-secondary float-right" onclick="GlobalClass.openModal('{{ route('access_control.create_role') }}', '')" >Create role</button></h2>
            <table class="table jquery-datatable">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;  foreach($roles as $key => $value){ ?>
                <tr class="active-row">
                    <td><?= $i ?></td>
                    <td><?= $value->name ?></td>
                    <td>
                        <form method="POST" action="{{ route('access_control.role_permissions') }}" class="role-permissions-form d-inline-flex">
                            @csrf
                            <input hidden value="{{ $value->id }}" name="role_id">
                            <button type="button" class="btn btn-primary role-permissions-button" data-role_id="{{ $value->id }}">Permissions</button>
                        </form>
                        <form method="POST" action="{{ route('access_control.destroy_role', ['role_id' => $value->id]) }}" class="d-inline-flex">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger delete-confirmation" type="button">Delete</button>
                        </form>
                    </td>
                </tr>
                <?php $i++; } ?>
                </tbody>
            </table>

        </div>
        <div class="col-sm-5 offset-1" id="permissions_div">
            <h2 class="align-content-center">Permissions<button class="btn btn-secondary float-right" onclick="GlobalClass.openModal('{{ route('access_control.create_permission') }}', '')" >Create permission</button></h2>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Active</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; foreach($permissions as $key => $value){ ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $value->name ?></td>
                    <td>
                        <input type="checkbox" class="role-permissions" value="{{ $value->id }}" data-url="{{ route('access_control.save_role_permission') }}">
                    </td>
                    <td>
                        <form method="POST" action="{{ route('access_control.destroy_permission', ['permission_id' => $value->id]) }}">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger delete-confirmation" type="button">Delete</button>
                        </form>
                    </td>
                </tr>
                <?php $i++; } ?>
                </tbody>
            </table>

        </div>

    </div>
@endsection

