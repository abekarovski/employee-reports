@section('modal-title', 'Create permission')
@include('includes.modal_header')
    <div class="container">
        <form type="POST" id="create_permission" class="submit-modal" action="{{route('access_control.store_permission')}}" data-message="Permission created successfully" data-button_id="application_access_control">
            <div class="row">
                <div class="col-sm-12 flex-space-between">
                    <label for="user_group_code" class="label-100 label-align-margin">Name:</label>
                    <input autocomplete="off" class="form-control" type="text" name="name" required="required">
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-sm-12">
                    <input class="btn btn-primary form-control" type="submit" value="Create" >
                </div>
            </div>
        </form>
    </div>
@include('includes.modal_footer')
<script>
    var token = '{{csrf_token()}}';
</script>



