@extends('layouts.master')

@section('content')
    <input hidden id="active_role_id" value="">
    <div class="row">
        <div class="col-sm-6">
            <h2 class="align-items-center">{{$user->name}}<span class="float-right">Roles</span></h2>
            <table class="table jquery-datatable">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;  foreach($roles as $key => $value){ ?>
                <tr class="active-row">
                    <td><?= $i ?></td>
                    <td><?= $value->name ?></td>
                    <td>
                        <input type="checkbox" class="user-role-permission" value="{{ $value->id }}"
                               @foreach($user->roles as $key1 => $value1)
                                   @if($value->id == $value1->id)
                                        {{ 'checked' }}
                                   @endif
                               @endforeach
                               data-url="{{ route('access_control.save_user_role') }}" data-user_id="{{ $user->id }}">
                    </td>
                </tr>
                <?php $i++; } ?>
                </tbody>
            </table>

        </div>
        <div class="col-sm-5 offset-1" id="permissions_div">
            <h2 class="align-content-center">Permissions</h2>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; foreach($permissions as $key => $value){ ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $value->name ?></td>
                    <td>
                        <input type="checkbox" class="user-role-permission" value="{{ $value->id }}"
                               @foreach($user->permissions as $key1 => $value1)
                                   @if($value->id == $value1->id)
                                        {{ 'checked' }}
                                   @endif
                               @endforeach
                               data-url="{{ route('access_control.save_user_permission') }}"
                               data-user_id="{{ $user->id }}">
                    </td>
                </tr>
                <?php $i++; } ?>
                </tbody>
            </table>

        </div>

    </div>
@endsection

