@extends('layouts.master')

@section('content')
    <div class="row p-lg-2 pt-5">
        <div class="col-sm-8 offset-2">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Add question</h3>
                </div>

                <div class="card-body">
                    <form method="post" action="{{route('questions.store')  }}">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12" >
                                <label for="question">Question:</label>
                                <input type="text" class="form-control" name="question" id="question" required="required">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="question_type">Type:</label>
                                <select class="form-control" id="question_type" name="type">
                                    <option value="input">Input</option>
                                    <option value="select">Select</option>
                                    <option value="textarea">Textarea</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row" id="question_select_group_div" hidden>
                            <div class="form-group col-md-12">
                                <label for="question_select_group">Group:</label>
                                <select class="form-control" id="question_select_group" name="question_select_group_id" disabled required>
                                    @foreach($question_groups as $key => $value)
                                        <option value="<?= $value->id ?>"><?= $value->name ?></option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary float-right">Add</button>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
