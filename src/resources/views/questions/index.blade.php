@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <h2 class="align-items-center">Questions <span class="float-right" style="color: <?= $active_questions != 10 ? 'red' : 'green' ?>">(Active {{ $active_questions }})</span></h2>
            <table class="table jquery-datatable">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Question</th>
                    <th scope="col">Type</th>
                    <th scope="col">Group</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;  foreach($questions as $key => $value){ ?>
                <tr style="background-color: <?= $value->currently_active == 1 ? '' : 'lightcoral' ?>">
                    <td><?= $i ?></td>
                    <td><?= $value->question ?></td>
                    <td><?= $value->type ?></td>
                    <td><?= $value->name ? $value->name : '' ?></td>
                    <td><a class="btn btn-primary" href="{{ route('questions.edit', ['question' => $value->id]) }}">Edit</a></td>
                    <td>
                        <?php if ($value->currently_active == 0){ ?>
                            <button class="btn btn-primary" onclick="GlobalClass.ajaxDataseCall('{{ route('questions.activate_question')}}', {'id': '<?= $value->id ?>', '_token' : '<?= csrf_token() ?>'} )">Activate</button>
                        <?php }else { ?>
                            <form method="POST" action="{{ route('questions.destroy', ['question' => $value->id]) }}">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger delete-confirmation" type="button">Deactivate</button>
                            </form>

                        <?php } ?>
                    </td>
                </tr>
                <?php $i++; } ?>
                </tbody>
            </table>

        </div>
        <div class="col-sm-5 offset-1">
            <h2 class="align-content-center">Question groups</h2>
            <table class="table jquery-datatable">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Code</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; foreach($question_groups as $key => $value){ ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $value->name ?></td>
                    <td><?= $value->code ?></td>
                    <td><a class="btn btn-primary" href="{{ route('questions.edit_select_group', ['question_select_group' => $value->id]) }}">Edit</a></td>
                </tr>
                <?php $i++; } ?>
                </tbody>
            </table>

        </div>

    </div>
@endsection

