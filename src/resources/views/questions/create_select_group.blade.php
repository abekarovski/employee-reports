@extends('layouts.master')

@section('content')
    <div class="row p-lg-2 pt-5">
        <div class="col-sm-8 offset-2">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Create question select group</h3>
                </div>

                <div class="card-body">
                    <form method="post" action="{{route('questions.store_question_select_group')  }}">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="question_group_name">Name:</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                                       id="question_group_name" required="required">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="question_group_code">Code:</label>
                                <input type="text" class="form-control @error('code') is-invalid @enderror" name="code"
                                       id="question_group_code" required="required">
                                @error('code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <hr>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <input type="text" class="form-control" id="question_select_name" placeholder="Label">
                            </div>
                            <div class="form-group col-md-5">
                                <input type="text" class="form-control" id="question_select_value" placeholder="Value">
                            </div>
                            <div class="form-group col-md-2">
                                <button class="btn btn-outline-primary" id="add_question_select" type="button">Add
                                </button>
                            </div>
                        </div>
                        <div class="form-row" id="add_question_selects_div">
                            <div class="form-group col-md-12">
                                <hr>
                            </div>
                        </div>
                        <div class="form-row" id="add_question_selects_div">
                            <div class="form-group col-md-12">
                                <hr>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary float-right">Create</button>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>

@endsection
