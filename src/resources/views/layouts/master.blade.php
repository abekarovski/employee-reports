<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Employee reports</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/jquery-ui.min.css">
    <link rel="stylesheet" href="/css/jquery-ui.structure.min.css">
    <link rel="stylesheet" href="/css/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="shortcut icon" href="{{ URL::asset('img/favicon.png') }}">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
        </ul>
        <!-- SEARCH FORM -->
        {{--
                <form class="form-inline ml-3">
                    <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                        <div class="input-group-append">
                            <button class="btn btn-navbar" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
        --}}

    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{ route('home') }}" class="brand-link">
            <img src="{{ URL::asset('img/logo.jpg') }}" alt="Employee reports Logo"
                 class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">Employee reports</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{ URL::asset('img/profile.png') }}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="{{ route('users.edit', ['user' => Auth::user()->id]) }}"
                       class="d-block">{{ Auth::user()->name }}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
          with font-awesome or any other icon font library -->
                    @can('questions.tab')
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-question"></i>
                                <p>
                                    Questions
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('questions.resource_routes')
                                    <li class="nav-item">
                                        <a href="{{ route('questions.index') }}" class="nav-link" id="list_questions">
                                            <i class="fas fa-list"></i>
                                            <p>List questions</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('questions.create') }}" class="nav-link">
                                            <i class="fas fa-plus-circle"></i>
                                            <p>Create</p>
                                        </a>
                                    </li>
                                @endcan
                                @can('question_selects.resource_routes')
                                    <li class="nav-item">
                                        <a href="{{ route('questions.create_select_group') }}" class="nav-link">
                                            <i class="fas fa-plus-circle"></i>
                                            <p>Create group</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcan
                    @can('reports.tab')
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-clipboard-list"></i>
                                <p>
                                    Reports
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('reports.resource_routes')
                                    <li class="nav-item">
                                        <a href="{{ route('reports.index') }}" class="nav-link">
                                            <i class="fas fa-list"></i>
                                            <p>All reports</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('reports.create') }}" class="nav-link">
                                            <i class="fas fa-plus-circle"></i>
                                            <p>New admin report</p>
                                        </a>
                                    </li>
                                @endcan
                                @can('reports.list_own_reports')
                                    <li class="nav-item">
                                        <a href="{{ route('reports.index_own_reports') }}" class="nav-link">
                                            <i class="fas fa-list-ul"></i>
                                            <p>My reports</p>
                                        </a>
                                    </li>
                                @endcan
                                @can('reports.create_employee_report')
                                    <li class="nav-item">
                                        <a href="{{ route('reports.create_employee_report') }}" class="nav-link">
                                            <i class="fas fa-plus-circle"></i>
                                            <p>New employee report</p>
                                        </a>
                                    </li>
                                @endcan
                                @can('reports.change_report_submission_time')
                                    <li class="nav-item">
                                        <a href="{{ route('reports.change_submission_time') }}" class="nav-link">
                                            <i class="fas fa-clock"></i>
                                            <p>Change submission time</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcan
                    @can('holidays.tab')
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-calendar-day"></i>
                                <p>
                                    Holidays
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('holidays.resource_routes')
                                    <li class="nav-item">
                                        <a href="{{ route('holidays.index') }}" class="nav-link" id="list_questions">
                                            <i class="fas fa-list"></i>
                                            <p>List holidays</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('holidays.create') }}" class="nav-link">
                                            <i class="fas fa-plus-circle"></i>
                                            <p>Add</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcan
                    @can('users.tab')
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    Employees
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('users.resource_routes')
                                    <li class="nav-item">
                                        <a href="{{route('users.index')}}" class="nav-link">
                                            <i class="fas fa-list"></i>
                                            <p>List all</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('users.create')}}" class="nav-link">
                                            <i class="fas fa-plus-circle"></i>
                                            <p>Create</p>
                                        </a>
                                    </li>
                                @endcan
                                @can('users.send_application_invite')
                                    <li class="nav-item">
                                        <a href="{{route('users.create_invitation')}}" class="nav-link">
                                            <i class="fas fa-envelope"></i>
                                            <p>Send invitation</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcan
                    @can('admin.tab')
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-cogs"></i>
                                <p>
                                    Admin
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('admin.application_access_control')
                                    <li class="nav-item">
                                        <a href="{{route('access_control.index')}}" class="nav-link">
                                            <i class="fas fa-tasks"></i>
                                            <p>Application access control</p>
                                        </a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcan
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout_form').submit();">
                            <i class="nav-icon fas fa-power-off"></i>
                            <p>
                                Log out
                            </p>
                        </a>
                        <form id="logout_form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid" id="app">
                @yield('content')
                <div class="display-modal"></div>
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
        </div>
        <!-- Default to the left -->
        <strong></strong>
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ URL::asset('js/app.js') }}"></script>
<script src="{{ URL::asset('js/script.js') }}"></script>
<script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
        $('.jquery-datatable').DataTable();
    });
    var token = '<?= csrf_token() ?>';
</script>
@include('sweetalert::alert')
<script src="{{ $cdn?? URL::asset('vendor/sweetalert/sweetalert.all.js')  }}"></script>

</body>
</html>
