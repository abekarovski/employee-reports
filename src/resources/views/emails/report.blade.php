<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

@component('mail::message')
# Introduction

<?php $questions = $data['questions']; ?>
@include('reports.answers')

Thanks,<br>
{{ config('app.name') }}
@endcomponent
