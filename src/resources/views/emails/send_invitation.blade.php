<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

@component('mail::message')
<h2>{{ $data['body'] }}</h2>
<br>
<h2>The link to the application registration form is: <a href="{{ route('register') }}" >{{ route('register') }}</a></h2>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
