<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

@component('mail::message')
<h1 class="font-weight-bold">Your report {{ $data['report']->name }} for week {{ $data['report']->date_week }} has been {{ $data['report']->approved == 1 ? 'approved!' : 'disapproved!'}}</h1>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
