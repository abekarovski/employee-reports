@extends('layouts.master')

@section('content')
    <div class="row p-lg-2 pt-5">
        <div class="col-sm-8 offset-2">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Create user</h3>
                </div>
                <form method="post" action="{{route('users.store')  }}">
                    @csrf
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="user_name">Name:</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror"
                                       id="user_name" name="name" required="required">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="user_email">Email</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror"
                                       id="user_email" name="email" required="required">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="user_password">Password:</label>
                                <input type="password" class="form-control @error('password') is-invalid @enderror"
                                       id="user_password" name="password" required="required">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="user_confirm_passwords">Confirm password:</label>
                                <input type="password" class="form-control" id="user_confirm_passwords"
                                       name="password_confirmation"
                                       required="required">
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary float-right">Create</button>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </form>
            </div>
        </div>
    </div>
@endsection
