@extends('layouts.master')

@section('content')
        <div class="row p-lg-2 pt-5">
            <div class="col-sm-8 offset-2">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">Invitation mail</h3>
                    </div>
                    <form method="post" action="{{route('users.send_invitation')  }}">
                        @csrf
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-8 offset-2">
                                    <label for="user_email">Email:</label>
                                    <input type="email" class="form-control @error('email') is-invalid @enderror"
                                           id="user_email" name="email" required="required">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-8 offset-2">
                                    <label for="email_content">Content:</label>
                                    <textarea class="form-control" id="email_content" name="body"></textarea>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary float-right">Send email</button>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </form>
                </div>
            </div>
        </div>
@endsection
