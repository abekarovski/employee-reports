@extends('layouts.master')

@section('content')
    <div class="row pt-5">
        <div class="col-sm-8 offset-2">
            <table class="table jquery-datatable">
                <thead class="thead-dark ">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Created at</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; foreach($users as $key => $value){ ?>
                <tr>
                    <th scope="row"><?= $i ?></th>
                    <td><?= $value->name ?></td>
                    <td><?= $value->email ?></td>
                    <td><?= $value->created_at ?></td>
                    <td><div class="d-inline-flex"><a href="{{ route('users.edit', ['user' => $value]) }}" class="btn btn-info">Edit</a>
                        <form method="POST" action="{{ route('users.destroy', ['user' => $value]) }}">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger delete-confirmation ml-2" type="button">Delete</button>
                        </form>
                        <a class="btn btn-primary  ml-2" href="{{ route('access_control.edit_users_access_control', ['user' => $value->id]) }}">Permissions</a>
                        </div>
                    </td>
                </tr>
                <?php $i++; } ?>
                </tbody>
            </table>
        </div>
    </div>
@endsection
