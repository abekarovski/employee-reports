@foreach($questions as $key => $value)
    <div class="form-row">
        <div class="form-group col-md-12">
            <div class="input-group mb-3">
                <input type="text" class="form-control" aria-label="Text input with checkbox" value="{{ $value->question }}" readonly>
                <div class="input-group-append">
                    <div class="input-group-text">
                        <input type="checkbox" aria-label="Checkbox for following text input" value="{{ $value->id }}" name="question_id[]" id="question_checkbox_{{ $value->id }}" {{ $value->currently_active ? 'checked' : '' }} class="questions-checkboxes">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
