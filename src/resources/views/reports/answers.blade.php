@foreach($questions as $key => $value)
    <div class="form-row">
        <div class="form-group col-md-5">
            <input type="text" class="form-control" aria-label="Text input with checkbox" value="{{ $value->question }}" readonly>
            <input type="text" value="{{ $value->id }}" name="question_id[]" hidden>
        </div>
        <div class="form-group col-md-7">
            @if($value->type == 'input')
                <input type="text" name="answer[]" class="form-control" required value="{{ $value->pivot->answer }}">
            @elseif($value->type == 'textarea')
                <textarea class="form-control" name="answer[]" required>{{ $value->pivot->answer }}</textarea>
            @else
                <select class="form-control" name="answer[]" >
                    @foreach($value->select_values as $key1 => $value1)
                        <option <?= $value1->id == $value->pivot->answer ? 'selected' : '' ?> value="{{ $value1->id }}">{{ $value1->name }}</option>
                    @endforeach
                </select>
            @endif
        </div>
    </div>
@endforeach
