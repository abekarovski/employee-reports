@extends('layouts.master')

@section('content')
    <div class="row p-lg-2 pt-5">
        <div class="col-sm-8 offset-2">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Report {{$report->name}}</h3>
                </div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="report_name">Report name:</label>
                            <input type="text" class="form-control" name="name" id="report_name" autocomplete="off" value="{{ $report->name }}" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="report_date">Year-week:</label>
                            <input type="text" class="form-control date-year-week" name="date_week" id="report_date" autocomplete="off" value="{{ $report->date_week }}" readonly/>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <hr>
                            <label>Questions:</label>
                        </div>
                    </div>
                    @foreach($report->questions as $key => $value)
                        <div class="form-row">
                            @if($report->type == 'questions')
                                <div class="form-group col-md-12">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" aria-label="Text input with checkbox" value="{{ $value->question }}" readonly>
                                    </div>
                                </div>
                            @else
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control" aria-label="Text input with checkbox" value="{{ $value->question }}" readonly>
                                </div>
                                <div class="form-group col-md-6">
                                    @if($value->type == 'input')
                                        <input type="text" name="answer[]" class="form-control" readonly value="{{ $value->pivot->answer }}">
                                    @elseif($value->type == 'textarea')
                                        <textarea class="form-control" name="answer[]" readonly>{{ $value->pivot->answer }}</textarea>
                                    @else
                                        <select class="form-control" name="answer[]" disabled >
                                            @foreach($value->select_values as $key1 => $value1)
                                                <option <?= $value1->id == $value->pivot->answer ? 'selected' : '' ?> value="{{ $value1->id }}">{{ $value1->name }}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
