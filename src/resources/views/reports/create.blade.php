@extends('layouts.master')

@section('content')
    <div class="row p-lg-2 pt-5">
        <div class="col-sm-8 offset-2">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Create report</h3>
                </div>

                    <div class="card-body">
                        <div class="form-row padding-top-8px">
                            <div class="form-group col-sm-10 offset-1">
                                <select id="copy_from_week_admin_reports" class="form-control" data-url="{{ route('reports.get_report_questions') }}">
                                    <option value="0">Copy questions from previous weeks</option>
                                    @foreach($reports as $key => $value)
                                        <option value="{{ $value->id }}">{{ $value->date_week.' | '.$value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-12">
                                <hr>
                            </div>
                        </div>
                        <form method="post" action="{{route('reports.store')  }}" id="create_report_form">
                            @csrf
                        <div class="form-row">
                            <div class="form-group col-sm-6">
                                <label for="report_name">Report name:</label>
                                <input type="text" class="form-control" name="name" id="report_name" autocomplete="off">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="report_date">Year-week(default next week):</label>
                                <input type="text" class="form-control date-year-week @error('date_week') is-invalid @enderror" name="date_week" id="report_date" autocomplete="off" value="{{ date('Y-W', strtotime('friday next week')) }}"/>
                                @error('date_week')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <input name="type" value="questions" hidden>
                                <input name="submitted" value="0" id="report_submitted" hidden>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-12">
                                <hr>
                                <label>Questions:</label>
                            </div>
                        </div>
                        @include('reports.questions')
                        <div class="card-footer">
                            <button type="button" class="btn btn-primary float-right m-1" id="submit_report">Submit</button>
                            <button type="button" class="btn btn-primary float-right m-1" id="create_report">Save</button>
                        </div>
                        </form>
                    </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
