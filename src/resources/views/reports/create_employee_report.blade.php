@extends('layouts.master')

@section('content')
    <div class="row p-lg-2 pt-5">
        <div class="col-sm-10 offset-1">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Create report</h3>
                </div>
                <div class="card-body">
                    <div class="form-row padding-top-8px">
                        <div class="form-group col-sm-10 offset-1">
                            <select id="copy_from_week_employee_reports" class="form-control"
                                    data-url="{{ route('reports.get_report_employee_questions') }}">
                                <option value="{{ date('Y-W', strtotime('friday this week')) }}">Fill out report for
                                    previous weeks
                                </option>
                                @foreach($reports as $key => $value)
                                    @if($value->date_week < date('Y-W', strtotime('friday this week')))
                                        <option
                                            value="{{ $value->date_week }}">{{ $value->date_week.' | '.$value->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-sm-12">
                            <hr>
                        </div>
                    </div>
                    @if($questions)
                        <form method="post" action="{{ route('reports.store') }}" id="create_report_form">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-sm-6">
                                    <label for="report_name">Report name:</label>
                                    <input type="text" class="form-control" name="name" id="report_name"
                                           autocomplete="off">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="report_date">Year-week(default current week):</label>
                                    <input type="text" class="form-control @error('date_week') is-invalid @enderror"
                                           name="date_week" id="report_date" autocomplete="off"
                                           value="{{ date('Y-W', strtotime('friday this week')) }}" readonly/>
                                    @error('date_week')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <input name="type" value="answers" hidden>
                                    <input name="submitted" value="0" id="report_submitted" hidden>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-sm-12">
                                    <hr>
                                    <label>Questions:</label>
                                </div>
                            </div>
                            <div id="report_answers_div">
                                @include('reports.answers')
                            </div>
                            <div class="card-footer">
                                <button type="button" class="btn btn-primary float-right m-1" id="submit_report">
                                    Submit
                                </button>
                                <button type="button" class="btn btn-primary float-right m-1" id="create_report">Save
                                </button>
                            </div>
                            @else
                                <div class="row">
                                    <div class="col-sm-8 offset-2">
                                        <h2 class="text-danger font-weight-bold">We are sorry to inform you that the
                                            admin hasn't submitted a valid form for this week yet. Please try again
                                            later.</h2>
                                    </div>
                                </div>
                            @endif
                        </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
