@extends('layouts.master')

@section('content')
    <div class="row pt-5">
        <div class="col-sm-5">
            <h2 class="align-items-center mb-2">Admin forms</h2>
            <table class="table jquery-datatable">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;  foreach($admin_reports as $key => $value){ ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $value->name ?></td>
                    <td><?= $value->date_week ?></td>
                    <td><a class="btn btn-info" href="{{ route('reports.show', ['report' => $value->id]) }}">Show</a>
                    </td>
                </tr>
                <?php $i++; } ?>
                </tbody>
            </table>
        </div>
        <div class="col-sm-6 offset-1">
            <h2 class="align-content-center mb-2">Employee reports</h2>
            <table class="table jquery-datatable">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">User</th>
                    <th scope="col">Name</th>
                    <th scope="col">Date</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;  foreach($employee_reports as $key => $value){ ?>
                <tr class="{{ $value->approved == 0 ? 'light-red' : '' }}">
                    <td><?= $i ?></td>
                    <td><?= $value->user_name ?></td>
                    <td><?= $value->name ?></td>
                    <td><?= $value->date_week ?></td>
                    <td>
                        <div class="d-inline-flex">
                            @if($value->approved === null)
                                <form method="POST" action="{{ route('reports.approve_report') }}">
                                    @csrf
                                    <input hidden name="approved" value="1">
                                    <input hidden name="id" value="{{ $value->id }}">
                                    <button class="btn btn-outline-primary" type="submit">Approve</button>
                                </form>
                                <form method="POST" action="{{ route('reports.approve_report') }}">
                                    @csrf
                                    <input hidden name="id" value="{{ $value->id }}">
                                    <input hidden name="approved" value="0">
                                    <button class="btn btn-outline-danger ml-2" type="submit">Disapprove</button>
                                </form>
                            @else
                                @can('reports.approve_report')
                                    @if($value->approved === 0)
                                        <span class="text-danger font-weight-bold">DISAPPROVED</span>
                                    @else
                                        <span class="text-success font-weight-bold">APPROVED</span>
                                    @endif
                                @endcan
                            @endif
                            <a class="btn btn-info ml-2" href="{{ route('reports.show', ['report' => $value->id]) }}">Show</a>
                        </div>
                    </td>
                </tr>
                <?php $i++; } ?>
                </tbody>
            </table>
        </div>
    </div>
@endsection
