@extends('layouts.master')

@section('content')
    <div class="row p-lg-2 pt-5">
        <div class="col-sm-8 offset-2">
            <div class="card card-dark">
                <div class="card-header">
                    <h3 class="card-title">Edit user</h3>
                </div>
                <form method="post" action="{{route('reports.store_submission_time')  }}" id="create_report_form">
                @csrf
                    <div class="card-body">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="submission_day">Submission report day:</label>
                                <select class="form-control" id="submission_day" name="submission_day">
                                    @foreach($week_days as $key => $value)
                                        <option
                                            value="{{ $value }}" {{ ($report_submission_time && $value==$report_submission_time->day) ? 'selected' : '' }}>{{ ucfirst($value) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="submission_time">Hour(number only):</label>
                                <input type="text" class="form-control @error('submission_time') is-invalid @enderror" name="submission_time" id="submission_time" autocomplete="off" value="{{ $report_submission_time->time ? explode(':', $report_submission_time->time)[0] : '' }}" required/>
                                @error('submission_time')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-8 offset-2">
                                <hr>
                                <label>Remainder times:</label>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <label for="remainder_oprator">Operator:</label>
                                <select class="form-control" id="remainder_oprator">
                                    <option value="+">+</option>
                                    <option value="-">-</option>
                                </select>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="remainder_hours">Hours:</label>
                                <input type="text" class="form-control" id="remainder_hours" autocomplete="off"/>
                            </div>
                            <div class="form-group col-md-2">
                                <button class="btn btn-outline-primary mt-md-4" type="button" id="add_remainder_time">Add</button>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <hr>
                            </div>
                        </div>
                        <div id="add_remainder_time_div">
                            @foreach($report_remainder_time as $key => $value)
                                <div class="form-row">
                                    <div class="form-group col-md-5">
                                        <input type="text" class="form-control" autocomplete="off" value="{{ $value->operator }}"
                                               name="operator[]" readonly/>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <input type="text" class="form-control" autocomplete="off" value="{{ $value->hours }}"
                                               name="hours[]" readonly/>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <button class="btn btn-danger delete-grandparent" type="button">Delete</button>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary float-right">Save</button>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </form>
            </div>
        </div>
    </div>
@endsection
