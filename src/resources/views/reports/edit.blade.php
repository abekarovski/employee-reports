@extends('layouts.master')

@section('content')
    <div class="row p-lg-2 pt-5">
        <div class="col-sm-12">
            <div class="card card-dark">
                <div class="card-header">
                    <h3 class="card-title">Edit report</h3>
                </div>
                <div class="card-body">
                    <div class="form-row padding-top-8px">
                        <div class="form-group col-md-10 offset-1">
                            <select id="copy_from_week_admin_reports" class="form-control"
                                    data-url="{{ route('reports.get_report_questions') }}">
                                <option value="0">Copy questions from previous weeks</option>
                                @foreach($reports as $key => $value)
                                    <option value="{{ $value->id }}">{{ $value->date_week.' | '.$value->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-sm-12">
                            <hr>
                        </div>
                    </div>
                    <form method="post" action="{{route('reports.update', ['report'=>$report->id])  }}"
                          id="create_report_form">
                        @method('patch')
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="report_name">Report name:</label>
                                <input type="text" class="form-control" name="name" id="report_name" autocomplete="off"
                                       value="{{ $report->name }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="report_date">Year-week:</label>
                                <input type="text" class="form-control date-year-week" name="date_week" id="report_date"
                                       autocomplete="off" value="{{ $report->date_week }}" readonly/>
                                <input name="type" value="questions" hidden>
                                <input name="submitted" value="{{ $value->submitted }}" id="report_submitted" hidden>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <hr>
                                <label>Questions:</label>
                            </div>
                        </div>
                        @include('reports.questions_edit')
                        <div class="card-footer">
                            <button type="button" class="btn btn-primary float-right m-1" id="submit_report">Submit
                            </button>
                            <button type="button" class="btn btn-primary float-right m-1" id="create_report">Save
                            </button>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>

@endsection
