@extends('layouts.master')

@section('content')
    <div class="row pt-5">
        <div class="col-sm-8 offset-2">
            <h2 class="align-content-center mb-2">Own reports</h2>
            <table class="table jquery-datatable">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Date</th>
                    <th scope="col">Type</th>
                    <th scope="col">Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1;  foreach($employee_reports as $key => $value){ ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $value->name ?></td>
                    <td><?= $value->date_week ?></td>
                    <td><?= $value->type == 'questions' ? 'Admin form' : 'Employee report' ?></td>
                    <td>
                        <?php switch ($value->code) {
                            case 'in_progress':
                                echo '<span class="badge badge-warning">' . $value->status_name . '</span>';
                                break;
                            case 'remainder_sent':
                                echo '<span class="badge badge-danger">' . $value->status_name . '</span>';
                                break;
                            case 'submitted':
                                echo '<span class="badge badge-primary">' . $value->status_name . '</span>';
                                break;
                            case 'approved':
                                echo '<span class="badge badge-success">' . $value->status_name . '</span>';
                                break;
                            default:
                                echo '<span class="badge badge-success">' . $value->status_name . '</span>';
                                break;
                        } ?>
                    </td>
                    <td>
                        <div class="d-inline-flex">
                            @if(!$value->submitted)
                                <a class="btn btn-primary mr-2"
                                   href="{{ route('reports.edit', ['report' => $value->id]) }}">Edit</a>
                                @can('reports.submit_report')
                                    <a class="btn btn-outline-primary mr-2"
                                       href="{{ route('reports.submit_report', ['report' => $value->id]) }}">Submit</a>
                                @endcan
                            @endif
                            <a class="btn btn-info"
                               href="{{ route('reports.show', ['report' => $value->id]) }}">Show</a>
                        </div>
                    </td>
                </tr>
                <?php $i++; } ?>
                </tbody>
            </table>
        </div>
    </div>
@endsection
