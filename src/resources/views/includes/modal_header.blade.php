<?php
/**
 * Created by PhpStorm.
 * User: aleksandar.b
 * Date: 27.12.2018
 * Time: 09:27
 */
?>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="margin: 0 auto">
            <div class="modal-header text-center" style="background-color: darkgrey">
                <h4 class="modal-title">@yield('modal-title')</h4>
                <button type="button" class="close" data-dismiss="modal" style="padding-right:15px;">&times;</button>
            </div>
            <div class="modal-body">
