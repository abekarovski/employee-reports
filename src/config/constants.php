<?php

return [
    'week_days' => [
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday',
        'sunday',
    ],

    'slack_webhook' => env('SLACK_WEBHOOK'),
];
