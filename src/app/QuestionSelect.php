<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionSelect extends Model
{
    public function question_select_group(){ // connects the user_groups to the access_rights table
        return $this->belongsTo('App\QuestionSelectGroup');
    }
}
