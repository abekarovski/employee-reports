<?php

namespace App\Console\Commands;

use App\Mail\ReportMail;
use App\Notifications\SendSlackRemainder;
use App\Report;
use App\ReportRemainderTime;
use App\ReportSubmissionTime;
use App\Status;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class sendMailRemainders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mail remainders to employees';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = array();
        $data['view'] = 'emails.send_report_remainder';//the email view
        $scheduled_report= ReportSubmissionTime::first();//get the set scheduled time, if not set friday by default
        $remainders = ReportRemainderTime::all();//get the set remainders, if not set the default
        $day = 'friday';
        $time = ' 09:00:00';

        if ($scheduled_report){
            $day = $scheduled_report->day;
            $time = ' '.$scheduled_report->time;
        }
        $current_date_week = date('Y-W', strtotime($day.' this week'));
        $current_date_hour = date('Y-m-d H').':00:00';
        $scheduled_date = check_date_for_holidays(date('Y-m-d', strtotime($day.' this week')).$time);//check if the scheduled date is a holiday, and if it is so return the first non holiday day
        $flag = 0;//flag that says weather a email is to be sent

        if (count($remainders) == 0){//default remainders if none are set
            switch ($current_date_hour) {
                case $scheduled_date:
                    $flag = 1;
                    $data['message'] = 'You report is due now. Please submit it!';
                    break;
                case date('Y-m-d H:i:s', strtotime($scheduled_date.'+1 hour')):
                    $flag = 1;
                    $data['message'] = 'You report is late 1 hour. Please submit it!';
                    break;
                case date('Y-m-d  H:i:s', strtotime($scheduled_date.'+2 hour')):
                    $flag = 1;
                    $data['message'] = 'You report is late 2 hours. Please submit it!';
                    break;
                case date('Y-m-d  H:i:s', strtotime($scheduled_date.'+4 hour')):
                    $flag = 1;
                    $data['message'] = 'You report is late 4 hours. Please submit it!';
                    break;
                case date('Y-m-d  H:i:s', strtotime($scheduled_date.'+24 hour')):
                    $flag = 1;
                    $data['message'] = 'You report is late 1 day. Please submit it!';
                    break;
                case date('Y-m-d  H:i:s', strtotime($scheduled_date.'+48 hour')):
                    $flag = 1;
                    $data['message'] = 'You report is late 2 days. Please submit it!';
                    break;
                default:
                    $flag = 0;
            }
        }else{//set remainders
            if ($scheduled_date == $current_date_hour){//check if the report is due now
                $flag = 1;
                $data['message'] = 'You report is due now. Please submit it!';
            }else{
                foreach ($remainders as $key => $value){//iterate through the remainders one by one
                    if ($current_date_hour == date('Y-m-d H:i:s', strtotime($scheduled_date.$value->operator.$value->hours.' hour'))){
                        if ($value->operator == '+'){
                            $flag = 1;
                            $data['message'] = 'You report is late '.$value->hours.' hours. Please submit it!';
                            break;
                        }else{
                            $flag = 1;
                            $data['message'] = 'You report is due in '.$value->hours.' hours. Please submit it!';
                            break;
                        }
                    }
                }
            }
        }

        if ($flag) {
            $users = User::all();
            foreach ($users as $key => $value) {

                $report = Report::where('submitted', '=', '1')
                    ->where('type', '=', 'answers')
                    ->where('user_id', '=', $value->id)
                    ->where('date_week', '=', $current_date_week)
                    ->first();//check weather a user hasnt submitted a report for the scheduled week



                if (empty($report)) {//send mail to users that havent submitted a report
                    echo 'test';
                    $report_unsubmitted = Report::where('submitted', '=', '0')
                        ->where('type', '=', 'answers')
                        ->where('user_id', '=', $value->id)
                        ->where('date_week', '=', $current_date_week)
                        ->first();

                    if (!empty($report_unsubmitted)){//change the report status to remainder send
                        DB::table('report_statuses')
                            ->where('report_id', $report_unsubmitted->id)
                            ->where('deleted_at', '=',null)
                            ->update(array('deleted_at' => now()));

                        $status = Status::where('code', 'remainder_sent')->first();

                        $report_unsubmitted->statuses()->attach($status->id);
                    }

                    Mail::to($value->email)->send(new ReportMail($data));
                    $value->notify(new SendSlackRemainder());
                }
            }

            return true;
        }
    }
}
