<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function question_select_group(){
        return $this->belongsTo('App\QuestionSelectGroup');
    }

    public function reports(){
        return $this->belongsToMany('App\Report', 'report_questions')->withPivot('answer')->withTimestamps();
    }
}
