<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionSelectGroup extends Model
{
    public function questions(){ // connects the user_groups to the access_rights table
        return $this->hasMany('App\Question');
    }

    public function question_selects(){ // connects the user_groups to the access_rights table
        return $this->hasMany('App\QuestionSelect');
    }
}
