<?php

namespace App\Http\Controllers;

use App\Mail\ReportMail;
use App\Question;
use App\QuestionSelectGroup;
use App\Report;
use App\ReportRemainderTime;
use App\ReportSubmissionTime;
use App\Status;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $admin_reports = Report::where('type', 'questions')->orderBy('date_week', 'DESC')->get();
        $employee_reports = DB::table('reports')
            ->join('users', 'users.id', '=', 'reports.user_id')
            ->select('reports.*', 'users.name as user_name')
            ->where('reports.type', '=', 'answers')
            ->where('reports.submitted', '=', '1')
            ->orderBy('date_week', 'DESC')
            ->get();

        return view('reports.index',[
            'admin_reports' => $admin_reports,
            'users' => $users,
            'post' => '',
            'employee_reports' => $employee_reports
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $questions = Question::all();
        $reports = Report::where('submitted', '=', '1')->where('type', '=', 'questions')->orderBy('date_week', 'DESC')->get();

        return view('reports.create', [
            'questions' => $questions,
            'reports' => $reports
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->type == 'questions'){
            $request->validate([
                'date_week'  => 'required|unique:reports,date_week,NULL,id,type,questions'
            ]);
        }else{
            $request->validate([
                'date_week'  => 'required|unique:reports,date_week,NULL,id,user_id,' . Auth::id().',type,answers'

            ]);
        }

        $report = new Report;

        $report->name = $request->name;
        $report->type = $request->type;
        $report->date_week = $request->date_week;
        $report->submitted = $request->submitted;
        $report->user_id = Auth::id();

        $report->save();

        if ($request->submitted){
            $status = Status::where('code', 'submitted')->first();

            $report->statuses()->attach($status->id);
        }else{
            $status = Status::where('code', 'in_progress')->first();

            $report->statuses()->attach($status->id);
        }

        foreach ($request->question_id as $key => $value){
            if ($request->answer[$key]){
                $report->questions()->attach($value, array('answer'=>$request->answer[$key]));
            }else{
                $report->questions()->attach($value);
            }
            if ($report->submitted == 1){
                $question = Question::findOrFail($value);

                $question->currently_active = 1;

                $question->save();
            }
        }

        return redirect(route('reports.index_own_reports'))->withSuccessMessage('Report successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report = Report::findOrFail($id);
        $report->questions = $report->questions()->get();
        foreach ($report->questions as $key => $value){
            if ($value->type == 'select'){
                $value->select_values = QuestionSelectGroup::findOrFail($value->question_select_group_id)->question_selects()->get();
            }
        }

        return view('reports.show', [
            'report'=>$report
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reports = Report::where('submitted', '=', '1')
            ->where('type', '=', 'questions')
            ->orderBy('date_week', 'DESC')
            ->get();

        $report = Report::findOrFail($id);

        if ($report->type == 'questions'){
            $questions = DB::select('SELECT questions.*,  report_questions.report_id
                    FROM questions
                    LEFT JOIN report_questions ON report_questions.question_id = questions.id AND report_questions.report_id= 8');

            return view('reports.edit', [
                'reports' => $reports,
                'report' => $report,
                'questions' => $questions
            ]);
        }else{
            $questions = $report->questions()->get();
            foreach ($questions as $key => $value){
                if ($value->type == 'select'){
                    $value->select_values = QuestionSelectGroup::findOrFail($value->question_select_group_id)->question_selects()->get();
                }
            }

            return view('reports.edit_employee_report', [
                'reports' => $reports,
                'report' => $report,
                'questions' => $questions
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $report = Report::findOrFail($id);

        $report->name = $request->name;
        $report->submitted = $request->submitted;

        $report->save();

        if ($request->submitted){
            DB::table('report_statuses')
                ->where('report_id', $id)
                ->where('deleted_at', '=',null)
                ->update(array('deleted_at' => now()));

            $status = Status::where('code', 'submitted')->first();

            $report->statuses()->attach($status->id);
        }

        $report->questions()->detach();
        foreach ($request->question_id as $key => $value){
            if ($request->answer[$key]){
                $report->questions()->attach($value, array('answer'=>$request->answer[$key]));
            }else{
                $report->questions()->attach($value);
            }
            if ($report->submitted == 1){
                $question = Question::findOrFail($value);

                $question->currently_active = 1;

                $question->save();
            }
        }

        return redirect(route('reports.index_own_reports'))->withSuccessMessage('Report successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        //
    }

    public function index_own_reports()
    {
        $employee_reports = DB::table('reports')
            ->join('users', 'users.id', '=', 'reports.user_id')
            ->join('report_statuses', 'report_statuses.report_id', '=', 'reports.id')
            ->join('statuses', 'statuses.id', '=', 'report_statuses.status_id')
            ->select('reports.*', 'users.name as user_name','statuses.name as status_name','statuses.code')
            ->where('reports.user_id', '=', Auth::id())
            ->where('report_statuses.deleted_at', '=', NULL)
            ->orderBy('date_week', 'DESC')
            ->get();

        return view('reports.index_own_reports',[
            'post' => '',
            'employee_reports' => $employee_reports
        ]);
    }

    public function submit_report($id){
        $report = Report::findOrFail($id);

        $report->submitted = 1;

        $report->save();

        DB::table('report_statuses')
            ->where('report_id', $id)
            ->where('deleted_at', '=',null)
            ->update(array('deleted_at' => now()));

        $status = Status::where('code', 'submitted')->first();

        $report->statuses()->attach($status->id);

        if($report->type == 'answers'){
            $questions = $report->questions()->get();
            foreach ($questions as $key => $value){
                if ($value->type == 'select'){
                    $value->select_values = QuestionSelectGroup::findOrFail($value->question_select_group_id)->question_selects()->get();
                }
            }

            $data = array(
                'view'=>'emails.report',
                'report'=>$report,
                'questions'=>$questions
            );

            Mail::to($report->user()->first()->email)->send(new ReportMail($data));
        }

        return redirect(route('reports.index_own_reports'))->withSuccessMessage('Report submitted successfully');
    }

    public function create_employee_report(){
        $reports = Report::where('submitted', '=', '1')
            ->where('type', '=', 'questions')
            ->orderBy('date_week', 'DESC')
            ->get();

        $report = Report::where('type', '=', 'questions')
            ->where('submitted', '=', 1)
            ->where('date_week', '=', date('Y-W', strtotime('friday this week')))
            ->first();

        $questions = '';
        if ($report){
            $questions = $report->questions()->get();
            foreach ($questions as $key => $value){
                if ($value->type == 'select'){
                    $value->select_values = QuestionSelectGroup::findOrFail($value->question_select_group_id)->question_selects()->get();
                }
            }
        }
        return view('reports.create_employee_report', [
            'reports' => $reports,
            'questions' => $questions
        ]);

    }

    public function get_report_questions(Request $request){
        if ($request->id){
            $questions = Report::findOrFail($request->id)->questions()->get();
        }else{
            $questions = Question::where('currently_active', '=', 1)->get();
        }

        echo json_encode($questions);
    }

    public function get_report_employee_questions(Request $request){
        $questions = '';
        $report = Report::where('date_week', '=', $request->date_week)
            ->where('user_id', Auth::id())
            ->first();

        if ($report){
            if ($report->submitted == 1){
                return 'submitted';
            }else{
                $questions = $report->questions()->get();
                foreach ($questions as $key => $value){
                    if ($value->type == 'select'){
                        $value->select_values = QuestionSelectGroup::findOrFail($value->question_select_group_id)->question_selects()->get();
                    }
                }

                $view = view('reports.answers', [
                    'questions' => $questions
                ]);
            }
        }else{
            $questions = Report::findOrFail($request->id)->questions()->get();
            foreach ($questions as $key => $value){
                if ($value->type == 'select'){
                    $value->select_values = QuestionSelectGroup::findOrFail($value->question_select_group_id)->question_selects()->get();
                }
            }

            $view = view('reports.answers', [
                'questions' => $questions
            ]);
        }

        return $view;
    }

/*    public function search_reports(Request $request){
        $date_week = $request->date_week;
        $user_id = $request->user_id;
        $users = User::all();
        $admin_reports = Report::where('type', 'questions')
            ->where(function ($query) use ($date_week, $user_id) {
                $query->when($date_week != '', function ($query) use ($date_week){
                    $query->where('reports.date_week', $date_week);
                });
                $query->when($user_id != '', function ($query) use ($user_id){
                    $query->where('reports.user_id', $user_id);
                });
            })
            ->orderBy('date_week', 'DESC')
            ->get();

        $employee_reports = DB::table('reports')
            ->join('users', 'users.id', '=', 'reports.user_id')
            ->select('reports.*', 'users.name as user_name')
            ->where('reports.type', '=', 'answers')
            ->where('reports.submitted', '=', '1')
            ->where(function ($query) use ($date_week, $user_id) {
                    $query->when($date_week != '', function ($query) use ($date_week){
                        $query->where('reports.date_week', $date_week);
                    });
                    $query->when($user_id != '', function ($query) use ($user_id){
                        $query->where('reports.user_id', $user_id);
                    });
            })
            ->orderBy('date_week', 'DESC')
            ->get();

        return view('reports.index',[
            'admin_reports' => $admin_reports,
            'users' => $users,
            'post' => $request,
            'employee_reports' => $employee_reports
        ]);
    }*/

/*    public function search_own_reports(Request $request)
    {
        $date_week = $request->date_week;

        $employee_reports = DB::table('reports')
            ->join('users', 'users.id', '=', 'reports.user_id')
            ->select('reports.*', 'users.name as user_name')
            ->where('reports.user_id', '=', Auth::id())
            ->where(function ($query) use ($date_week) {
                $query->when($date_week != '', function ($query) use ($date_week){
                    $query->where('reports.date_week', $date_week);
                });
            })
            ->orderBy('date_week', 'DESC')
            ->get();

        return view('reports.index_own_reports',[
            'post' => $request,
            'employee_reports' => $employee_reports
        ]);
    }*/

    public function change_submission_time(){
        $report_submission_time = ReportSubmissionTime::first();
        $report_remainder_time = ReportRemainderTime::all();

        $week_days = Config::get('constants.week_days');

        return view('reports.change_submission_time', [
            'report_submission_time'=>$report_submission_time,
            'week_days'=>$week_days,
            'report_remainder_time'=>$report_remainder_time
        ]);
    }

    public function store_submission_time(Request $request){
        $request->validate([
            'submission_time' => 'required|numeric|max:24'
        ]);

        ReportSubmissionTime::truncate();
        ReportRemainderTime::truncate();

        $report_submission = new ReportSubmissionTime;

        $report_submission->day = $request->submission_day;
        $report_submission->time = sprintf('%02d', $request->submission_time).':00:00';

        $report_submission->save();

        foreach ($request->hours as $key => $value){
            $report_remainders = new ReportRemainderTime;

            $report_remainders->operator = $request->operator[$key];
            $report_remainders->hours = $value;

            $report_remainders->save();
        }

        return redirect(route('home'))->withSuccessMessage('Report submission time and remainders successfully changed');
    }

    public function approve_report(Request $request){
        $report = Report::findOrFail($request->id);

        $report->approved = $request->approved;

        $report->save();

        DB::table('report_statuses')
            ->where('report_id', $request->id)
            ->where('deleted_at', '=',null)
            ->update(array('deleted_at' => now()));

        $status = Status::where('code', 'approved')->first();
        $report->statuses()->attach($status->id);

        $data = array(
            'view'=>'emails.report_approval',
            'report'=>$report,
        );

        Mail::to($report->user()->first()->email)->send(new ReportMail($data));

        return redirect(route('reports.index'))->withSuccessMessage('Report '.$request->approved==1 ? "approved" : "disapproved".' successfully');
    }



}
