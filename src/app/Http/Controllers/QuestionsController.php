<?php

namespace App\Http\Controllers;

use App\Question;
use App\QuestionSelect;
use App\QuestionSelectGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DebugBar\DebugBar;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Validation\Rule;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $questions = DB::table('questions')
            ->leftJoin('question_select_groups', 'question_select_groups.id', '=', 'questions.question_select_group_id')
            ->select('questions.*', 'question_select_groups.name')
            ->get();
        $questionGroups = QuestionSelectGroup::all();
        $active_questions = Question::where('currently_active', 1)->count();


        return view('questions.index', [
            'questions' => $questions,
            'active_questions' => $active_questions,
            'question_groups' => $questionGroups
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $questionGroups = QuestionSelectGroup::all();

        return view('questions.create', [
            'question_groups' => $questionGroups
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $question = new Question;

        $question->question = request('question');
        $question->type = request('type');
        if (request('question_select_group_id'))
            $question->question_select_group_id = request('question_select_group_id');

        $question->save();

        return redirect(route('questions.index'))->withSuccessMessage('Question successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::findOrFail($id);
        $questionGroups = QuestionSelectGroup::all();

        return view('questions.edit', [
            'question' => $question,
            'question_groups' => $questionGroups
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $question = Question::findOrFail($id);

        $question->question = $request->question;
        $question->type = $request->type;
        if ($request->question_select_group_id)
            $question->question_select_group_id = $request->question_select_group_id;
        else
            $question->question_select_group_id = null;

        $question->save();

        return redirect(route('questions.index'))->withSuccessMessage('Question successfully created!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::findOrFail($id);

        $question->currently_active = 0;

        $question->save();

        return redirect(route('questions.index'))->withSuccessMessage('Question deactivated successfully!');
    }

    public function activate_question(Request $request)
    {
        $question = Question::findOrFail(request('id'));

        $question->currently_active = 1;

        $question->save();

        Alert::toast('Question activated successfully!', 'success');
    }


    public function create_select_group()
    {
        return view('questions.create_select_group');
    }

    public function store_question_select_group(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'code' => ['required', 'string', 'max:255', 'unique:question_select_groups'],
        ]);

        if (!isset($request->question_select_label) || count($request->question_select_label) < 2) {
            Alert::error('Error', 'Enter atleast two values for the select!');

            return redirect(route('questions.create_select_group'));
        }

        $question_group = new QuestionSelectGroup;

        $question_group->name = $request->name;
        $question_group->code = $request->code;

        $question_group->save();

        foreach ($request->question_select_label as $key => $value) {
            $question_select = new QuestionSelect;

            $question_select->name = $value;
            $question_select->value = $request->question_select_value[$key];
            $question_select->question_select_group_id = $question_group->id;

            $question_select->save();
        }

        return redirect(route('questions.index'))->withSuccessMessage('Question select group added successfully!');
    }

    public function edit_select_group($id)
    {
        $question_group = QuestionSelectGroup::findOrFail($id);
        $question_selects = QuestionSelect::where('question_select_group_id', $id)->orderBy('value')->get();

        return view('questions.edit_select_group', [
            'question_group' => $question_group,
            'question_selects' => $question_selects,
        ]);
    }

    public function update_question_select_group(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'code' => ['required', 'string', 'max:255', Rule::unique('question_select_groups')->ignore($id)],
        ]);

        $question_group = QuestionSelectGroup::findOrFail($id);

        $question_group->name = $request->name;
        $question_group->code = $request->code;

        $question_group->save();

        foreach ($request->question_select_label as $key => $value) {
            $question_select = new QuestionSelect;

            $question_select->name = $value;
            $question_select->value = $request->question_select_value[$key];
            $question_select->question_select_group_id = $question_group->id;

            $question_select->save();
        }

        return redirect(route('questions.index'))->withSuccessMessage('Question select group updated successfully!');
    }


}
