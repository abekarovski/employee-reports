<?php

namespace App\Http\Controllers;

use App\Holiday;
use Illuminate\Http\Request;

class HolidaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $holidays = Holiday::where('year', '=', date('Y'))
            ->orWhere('type', '=', 'global')
            ->orderBy('month_day')
            ->get();

        return view('holidays.index', [
            'holidays' => $holidays
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('holidays.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->date) && !empty($request->date)){
            foreach ($request->date as $key => $value){
                $holiday = new Holiday;

                $holiday->name = $request->name[$key];
                $holiday->month_day = date('m-d', strtotime($value));
                $holiday->year = date('Y', strtotime($value));
                $holiday->type = $request->type[$key];

                $holiday->save();
            }
        }else{
            return redirect(route('holidays.create'));
        }

        return redirect(route('holidays.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Holiday::findOrFail($id)->delete();

        return redirect(route('holidays.index'));
    }
}
