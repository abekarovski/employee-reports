<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AccessControlController extends Controller
{
    public function index(){
        $roles = Role::orderBy('name', 'ASC')->get();
        $permissions = Permission::orderBy('name', 'ASC')->get();

        return view('admin.application_access_control_index', [
            'roles' => $roles,
            'permissions' => $permissions,
        ]);
    }

    public function role_permissions(Request $request){
        $permissions = Role::findById($request->role_id)->permissions()->get();

        echo json_encode($permissions);
    }

    public function save_role_permission(Request $request){
        if ($request->save == 'true'){
            Role::findById($request->role_id)->permissions()->attach($request->permission_id);
        }else{
            Role::findById($request->role_id)->permissions()->detach($request->permission_id);
        }
    }

    public function create_role(Request $request){
        $view = view('admin/create_role')->render();
        echo json_encode($view);
    }

    public function store_role(Request $request){
        $request->validate([
            'name' => 'required|unique:roles'
        ]);

        Role::create(['name' => $request->name]);
    }

    public function destroy_role($id){
        Role::findById($id)->delete();

        return redirect(route('access_control.index'));
    }

    public function create_permission(Request $request){
        $view = view('admin/create_permission')->render();
        echo json_encode($view);
    }

    public function store_permission(Request $request){
        $request->validate([
            'name' => 'required|unique:permissions'
        ]);


        Permission::create(['name' => $request->name]);
    }

    public function destroy_permission($id){
        Permission::findById($id)->delete();

        return redirect(route('access_control.index'));
    }

    public function edit_users_access_control($id){
        $user = User::findOrFail($id);
        $user->roles = $user->roles()->get();
        $user->permissions = $user->permissions()->get();

        $roles = Role::orderBy('name', 'ASC')->get();
        $permissions = Permission::orderBy('name', 'ASC')->get();

        return view('admin.edit_user_access_control', [
            'roles' => $roles,
            'user' => $user,
            'permissions' => $permissions,
        ]);
    }

    public function save_user_role(Request $request){
        if ($request->save == 'true'){
            User::findOrFail($request->user_id)->roles()->attach($request->id);
        }else{
            User::findOrFail($request->user_id)->roles()->detach($request->id);
        }
    }

    public function save_user_permission(Request $request){
        if ($request->save == 'true'){
            User::findOrFail($request->user_id)->permissions()->attach($request->id);
        }else{
            User::findOrFail($request->user_id)->permissions()->detach($request->id);
        }
    }
}
