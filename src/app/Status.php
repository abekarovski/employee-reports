<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public function reports(){
        return $this->belongsToMany('reports', 'report_statuses')
            ->whereNull('report_statuses.deleted_at')
            ->withPivot('deleted_at')
            ->withTimestamps();
    }
}
