<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function questions(){
        return $this->belongsToMany('App\Question', 'report_questions')
            ->withPivot('answer')
            ->withTimestamps();
    }

    public function statuses(){
        return $this->belongsToMany('App\Status', 'report_statuses')
            ->whereNull('report_statuses.deleted_at')
            ->withPivot('deleted_at')
            ->withTimestamps();
    }
}
