<?php
/**
 * Created by PhpStorm.
 * User: aleksandar.b
 * Date: 25.12.2018
 * Time: 09:16
 */

if (! function_exists('check_date_for_holidays')){

    function check_date_for_holidays($date){//recursion that returns the first non-holiday day going backwards
        $month_day = date('m-d', strtotime($date));
        $holiday = \App\Holiday::where('month_day', '=', $month_day)->first();
        if ($holiday) {
            if ($holiday->type == 'global') {
                return check_date_for_holidays(date('Y-m-d H:i:s', strtotime(date('Y-m-d  H:i:s', strtotime($date.'-24 hour')))));
            }
            else{
                $holiday1 = \App\Holiday::where('month_day', '=', $month_day)
                    ->where('year', '=',  date('Y'))->first();

                if ($holiday1) {
                    return check_date_for_holidays(date('Y-m-d H:i:s', strtotime(date('Y-m-d  H:i:s', strtotime($date.'-24 hour')))));
                }

            }
        }

        return $date;
    }
}
